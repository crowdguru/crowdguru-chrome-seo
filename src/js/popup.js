chrome.tabs.executeScript(null, {
    file: "js/script.js"
});

var pageTitle = [];

var notAvailable = 'Not available',
    contentMissing = 'Content is missing';

function parseQuery(queryString) {
    var query = {};
    var pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
    for (var i = 0; i < pairs.length; i++) {
        var pair = pairs[i].split('=');
        query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    }
    return query;
}

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        // Get the current active tab in the lastly focused window
        chrome.tabs.query({
            active: true,
            lastFocusedWindow: true
        }, function(tabs) {
            sendResponse({})

            // and use that tab to fill in url and all page information
            var tab = tabs[0],
                tabURL = tab.url;

            let settings = tabURL.split('?')[1];

            let query = parseQuery(settings);

            // Page Title
            var pageTitleElem = document.getElementById("page-title"),
                titleElem = document.createElement("p"),
                pageTitle = request.pageTitle;

            if (pageTitle == notAvailable || pageTitle == contentMissing) {
                titleElem.innerHTML = '<span class="missing-value">' + pageTitle + '</span>';
            } else {
                titleElem.innerText = pageTitle;
            }
            pageTitleElem.appendChild(titleElem);


            let domain = document.getElementById("domain");
            domain.value = query.domain;

            let position = document.getElementById("position");

            let found = "nicht unter den ersten 100";

            let results = request.results;
            results.forEach(function(e, i) {
                if (e.indexOf(query.domain.trim()) != -1) {
                    found = i;
                }
            });


            position.innerHTML = found + 1;


            var xhr = new XMLHttpRequest();

            xhr.onreadystatechange = function() {
                if (xhr.readyState === 4) {
                    if (typeof tab != 'undefined') {

                        // Show content
                        infoElem.style.display = "block";

                        // Hide spinner
                        document.getElementById("spinner").style.display = "none";

                    }
                }
            };

            xhr.open('GET', tabURL, true);
            xhr.send(null);

            // Hide content
            var infoElem = document.getElementById("info");
            infoElem.style.display = "none";

            // Show spinner
            spinnerElem = document.getElementById("spinner");
            spinnerElem.style.display = "block";

            return true;
        });

    });