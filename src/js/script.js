var notAvailable = 'Not available',
    contentMissing = 'Content is missing';

function getLinkElement(rel) {
    var links = document.getElementsByTagName('link'),
        found = notAvailable;

    for (var i = 0, l; l = links[i]; i++) {
        if (l.getAttribute('rel') === rel) {
            found = l.getAttribute('href');
            if (!found) {
                return contentMissing;
            } else {
                return found;
            }
        }
    }
    return found;
}



function getElement(name) {
    var elements = document.getElementsByTagName(name);
    for (var i = 0, eLen = elements.length; i < eLen; i++) {
        if (elements[0].innerText === '') {
            return contentMissing;
        } else {
            return elements[0].innerText;
        }
    }
}



var pageTitle = [],
    getPageTitle = (typeof getElement('title') !== "undefined") ? getElement('title').replace(/\s+/g, ' ') : notAvailable;
pageTitle.push(getPageTitle);

var l = [];

document.querySelectorAll("h3").forEach(e => l.push(e.closest(".g a")));
var results = l.filter(e => e).map(e => e.getAttribute("href"));

chrome.runtime.sendMessage({
    pageTitle: pageTitle,
    results: results
}, function(response) {});